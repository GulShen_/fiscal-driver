import 'package:fiscal_driver/category_item.dart';
import 'package:flutter/material.dart';

class CategoriesScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Fiscal Driver"),
      ),
      body: GridView(
        padding: EdgeInsets.all(10),
        gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
          maxCrossAxisExtent: 300, //поперечная ось
          childAspectRatio: 3 / 2,
          crossAxisSpacing: 10, //
          mainAxisSpacing: 20, //
        ),
        children: [
          CategoryItem(1,"Get Info"),
          CategoryItem(2,"Login"),
          CategoryItem(3,"Open Shift"),
          CategoryItem(4,"Sale"),
          CategoryItem(5,"Credit"),
          CategoryItem(6,"Credit Payment"),
          CategoryItem(7,"Avans"),
          CategoryItem(8,"Money back"),
          CategoryItem(9,"X Report"),
          CategoryItem(10,"Close Shift"),
          // CategoryItem("Parameters"),
        ],
      ),
    );
  }
}

import 'package:fiscal_driver/dialog/open_shiftdialog.dart';
import 'package:fiscal_driver/dialog/responce_dialog.dart';
import 'package:fiscal_driver/screens/sale_screen.dart';
import 'package:flutter/material.dart';

class CategoryItem extends StatelessWidget {
  final String title;
  final int id;
  CategoryItem(this.id, this.title);

  void clickk() {}

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        if (id == 1 || id == 2) {
          showDialog(
              context: context,
              builder: (BuildContext ctx) {
                return ResponceDialog(
                  title: title,
                  content: "responce",
                  btnText: "OK",
                );
              });
        } else if (id == 3) {
          showDialog(
              context: context,
              builder: (BuildContext ctx) {
                return OpenShiftDialog();
              });
        } else if (id == 4) {
          Navigator.of(context).push(MaterialPageRoute(builder: (_) {
            return SaleScreen();
          }));
        }
      },
      splashColor: Theme.of(context).primaryColor,
      borderRadius: BorderRadius.circular(15),
      child: Container(
        padding: const EdgeInsets.all(15),
        child: Text(
          title,
          style: Theme.of(context).textTheme.headline6,
        ),
        decoration: BoxDecoration(
          gradient: LinearGradient(
              colors: [Colors.blue.shade900, Colors.blue],
              begin: Alignment.topLeft,
              end: Alignment.bottomRight),
          borderRadius: BorderRadius.circular(15),
        ),
      ),
    );
  }

  // Future _showAlert(BuildContext context) {
  //   return showDialog(
  //       context: context,
  //       builder: (BuildContext context) {
  //         return AlertDialog(
  //           title: Text(
  //             "Tokent Info",
  //             style: TextStyle(color: Colors.blue[900]),
  //           ),
  //           content: Text("token issss"),
  //           actions: [
  //             FlatButton(
  //                 onPressed: () {
  //                   Navigator.of(context).pop();
  //                 },
  //                 child: Text(
  //                   "OK",
  //                   style: TextStyle(color: Colors.blue[900]),
  //                 ))
  //           ],
  //         );
  //       });
  // }
}

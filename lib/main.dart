import 'package:flutter/material.dart';

import 'categories_screen.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    MaterialColor colorCustom = MaterialColor(0xFF0D47A1, color);

    return MaterialApp(
      title: 'DeliMeals',
      theme: ThemeData(
          primarySwatch: colorCustom,
          accentColor: Colors.yellow,
          canvasColor: Colors.white,
          fontFamily: 'Raleway',
          textTheme: ThemeData.dark().textTheme.copyWith(
                bodyText1: TextStyle(color: Colors.green),
                bodyText2: TextStyle(color: Colors.black),
                headline6: TextStyle(
                    fontSize: 17,
                    color: Colors.white,
                    fontFamily: 'RobotoCondensed',
                    fontWeight: FontWeight.bold),
              )),
      home: CategoriesScreen(),
      initialRoute: '/',
      routes: {
        // '/': (ctx) => CategoriesScreen(),
        // CategoryMealsScreen.routeName: (ctx) => CategoryMealsScreen(),
        // MealDetailScreen.routeName: (ctx) => MealDetailScreen(),
      },
    );
  }

  final Map<int, Color> color = {
    50: Color.fromRGBO(136, 14, 79, .1),
    100: Color.fromRGBO(136, 14, 79, .2),
    200: Color.fromRGBO(136, 14, 79, .3),
    300: Color.fromRGBO(136, 14, 79, .4),
    400: Color.fromRGBO(136, 14, 79, .5),
    500: Color.fromRGBO(136, 14, 79, .6),
    600: Color.fromRGBO(136, 14, 79, .7),
    700: Color.fromRGBO(136, 14, 79, .8),
    800: Color.fromRGBO(136, 14, 79, .9),
    900: Color.fromRGBO(136, 14, 79, 1),
  };
}

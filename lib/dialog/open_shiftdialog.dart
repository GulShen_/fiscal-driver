import 'package:flutter/material.dart';

class OpenShiftDialog extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Dialog(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8)),
      child: Stack(
        overflow: Overflow.visible,
        alignment: Alignment.topCenter,
        children: [
          Container(
            height: 170,
            child: Column(
              children: [
                Padding(
                  padding: const EdgeInsets.fromLTRB(15, 40, 15, 15),
                  child: Container(
                    // width: 200,
                    height: 50,
                    child: TextField(
                      style: TextStyle(color: Colors.black, fontSize: 17),
                      decoration: InputDecoration(
                          border: OutlineInputBorder(), hintText: "Clerk name"),
                    ),
                  ),
                ),
                Spacer(),
                Align(
                    alignment: Alignment.bottomRight,
                    child: FlatButton(
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                        child: Text(
                          "Ok",
                          style: TextStyle(fontSize: 17),
                        )))
              ],
            ),
          ),
          Positioned(
            top: -30,
            child: CircleAvatar(
              radius: 30,
              backgroundColor: Colors.blue,
              child: Icon(
                Icons.person_pin,
                color: Colors.white,
                size: 40,
              ),
            ),
          )
        ],
      ),
    );
  }
}

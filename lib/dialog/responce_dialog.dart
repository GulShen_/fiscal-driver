import 'package:flutter/material.dart';

class ResponceDialog extends StatelessWidget {
  final String title;
  final String content;
  final String btnText;

  ResponceDialog(
      {@required this.title, @required this.content, @required this.btnText}) {
    print(title);
  }

  @override
  Widget build(BuildContext context) {
    return Dialog(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8)),
      child: Stack(
        overflow: Overflow.visible,
        alignment: Alignment.topCenter,
        children: [
          Container(
            height: 200,
            child: Padding(
              padding: const EdgeInsets.fromLTRB(10, 30, 10, 10),
              child: Column(
                children: [
                  Text(
                    title,
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 17,
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Text(content),
                  SizedBox(
                    height: 10,
                  ),
                  Spacer(),
                  Align(
                    alignment: Alignment.bottomRight,
                    child: FlatButton(
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                        child: Text(
                          btnText,
                          style: TextStyle(
                              color: Colors.blue,
                              // fontWeight: FontWeight.bold,
                              fontSize: 20),
                        )),
                  )
                ],
              ),
            ),
          ),
          Positioned(
              top: -30,
              child: CircleAvatar(
                backgroundColor: Colors.blue,
                radius: 30,
                child: Icon(
                  Icons.info_outlined,
                  color: Colors.white,
                  size: 50,
                ),
              )),
        ],
      ),
    );
  }
}
